package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"lab/blog/internal/health"
	"lab/blog/internal/home"

	"github.com/gorilla/mux"
	"github.com/juju/errors"
	_ "github.com/lib/pq"
)

func main() {
	serverAddress := ":4001"

	fmt.Printf("Trying to connect to db through %s:%s.\n", os.Getenv("DB_HOST"), os.Getenv("DB_PORT"))

	homeHandler := home.NewHandler()

	r := mux.NewRouter()

	r.HandleFunc("/", homeHandler.HandleFunc)
	r.HandleFunc("/health", health.HandleFunc)

	server := &http.Server{
		Handler:           r,
		Addr:              serverAddress,
		ReadHeaderTimeout: 3 * time.Second,
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		fmt.Println("Listening on", serverAddress)
		if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("Something went wrong with the server: %s\n", err)
		}
	}()

	<-done
	log.Print("Server has stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Printf("Something went wrong while server was shutting down:%+v\n", err)
		return
	}
	log.Print("Server stopped properly")
}
