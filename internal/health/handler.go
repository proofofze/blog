package health

import (
	"fmt"
	"net/http"
)

func HandleFunc(w http.ResponseWriter, _ *http.Request) {
	_, _ = fmt.Fprintf(w, "I am healthy!")
}
