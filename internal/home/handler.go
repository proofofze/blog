package home

import (
	"html/template"
	"log/slog"
	"net/http"
)

type Data struct {
	Page string
}

type Handler struct{}

func NewHandler() *Handler {
	return &Handler{}
}

func (h *Handler) HandleFunc(w http.ResponseWriter, _ *http.Request) {
	tmpl := template.Must(template.ParseFiles(
		"./web/index.gohtml",
		"./web/header.gohtml",
		"./web/navbar.gohtml",
	))

	data := Data{
		Page: "home",
	}

	if err := tmpl.Execute(w, data); err != nil {
		slog.Error(err.Error(), "error", err)
		http.Error(w, "something went deadly wrong...", http.StatusInternalServerError)
		return
	}
}
