FROM golang:1.21 as builder

WORKDIR /app

# Get dependencies
COPY go.mod ./
RUN go mod download

# Copying all the files to build the binaries
COPY . .

# Build it for linux OS
RUN CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -a -o ./bin/blog ./cmd/blog/main.go

FROM alpine

# Exposing server port
EXPOSE 4001

COPY --from=builder /app/bin/blog /blog
COPY --from=builder /app/web/ /web/

CMD ["/blog"]