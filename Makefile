.PHONY: test fmt vet

PROJECT_NAME := blog
FOLDER_BIN = $(CURDIR)/bin
COMMIT_SHA = $(shell git rev-parse --short HEAD)
TAG_COMMIT_SHA = $(PROJECT_NAME):$(COMMIT_SHA)
TAG_LATEST = $(PROJECT_NAME):latest
GO_TEST = go test ./...
GO_FILES = $(shell go list ./... | grep -v /vendor/)

dev:
	air

start:
	go run cmd/blog/main.go

build:
	go build -o $(FOLDER_BIN)/$(PROJECT_NAME) $(CURDIR)/cmd/blog/main.go

test:
	$(GO_TEST) -coverprofile cover.out

test-race:
	$(GO_TEST) -race

fmt:
	go fmt $(GO_FILES)

vet:
	go vet $(GO_FILES)

test-all: fmt vet test-race

mod:
	go mod tidy

docker-build:
	docker build --tag $(TAG_LATEST) --tag $(TAG_COMMIT_SHA) .

docker-run:
	(&>/dev/null docker run -p 4001:4001 blog &)

docker-stop:
	docker stop $(shell docker ps | grep blog | awk '{ print $$1 }')
